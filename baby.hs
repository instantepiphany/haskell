import Data.List

divPlates :: Float -> Float
divPlates x = x / 212


lucky :: (Integral a) => a -> String
lucky 7 = "U GOT DA RUCKY RUN"
lucky x = "AHHH SO UNRUCKY!!!"


addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

head' :: [a] -> a
head' [] = error "Can't call head on an empty list, dummy!"
head' (x:_) = x 


initials :: String -> String -> String
initials firstname lastname = [f] ++ "." ++ [l] ++ "."
    where (f:_) = firstname
          (l:_) = lastname

secondInitials :: String -> String -> String
secondInitials firstname lastname = [f] ++ "." ++ [l] ++ "."
    where (_k:f:_) = firstname
          (_s:l:_) = lastname

describeList :: [a] -> String
describeList xs = "The list is " ++ what xs
    where what [] = "empty"
          what [x] = "a singleton list."
          what [y,x] = "a list of two unique members."
          what xs = "a longer list."


checkCute :: String -> String
checkCute "Alisha" = "Cuteness: over 9000"
checkCute x = "Nope.... No cuteness here"

maximum' :: (Ord a) => [a] -> a
maximum' [] = error "maximum of empty list"
maximum' [x] = x 
maximum' (x:xs)
  | x > maxTail = x
  | otherwise = maxTail
  where maxTail = maximum' xs

compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare x 100

multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z


numUniques :: (Eq a) => [a] -> Int
numUniques = length . nub

testFunction :: String -> String
testFunction "test" = "Anything goes here!"
